#include <Arduino.h>
#include <FanController.h>

#define SENSOR_PIN 2

#define SENSOR_THRESHOLD 1000
#define PWM_PIN 9

FanController fan(SENSOR_PIN, SENSOR_THRESHOLD, PWM_PIN);

void setup(void)
{
  Serial.begin(9600);
  Serial.println("Fan Controller Library Demo");

  fan.begin();
}

void loop(void)
{
  Serial.print("Current speed: ");
  unsigned int rpms = fan.getSpeed();
  Serial.print(rpms);
  Serial.println("RPM");

  if (Serial.available() > 0) {
    int input = Serial.parseInt();

    byte target = max(min(input, 100), 0);

    Serial.print("Setting duty cycle: ");
    Serial.println(target, DEC);

    // Set fan duty cycle
    fan.setDutyCycle(target);
  }
  delay(250);
}